\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{uebblatt}[2013/03/29 LaTeX class]

\DeclareOption{entwurf}{\AtEndOfClass{
  \RequirePackage{draftwatermark}
  \definecolor{pink}{rgb}{0.95,0.9,0.95}
  \SetWatermarkText{\textsf{\textcolor{pink}{ENTWURF}}}
  \SetWatermarkScale{1}
}}

\ProcessOptions\relax

\LoadClass[a4paper,ngerman]{scrartcl}
\RequirePackage{etex}

\RequirePackage{ifxetex}
\ifxetex\else\RequirePackage[utf8]{inputenc}\fi
\RequirePackage[ngerman]{babel}
\RequirePackage{amsmath,amsthm,amssymb,amscd,bussproofs,stmaryrd,color,graphicx,environ,tabto,xspace}
\RequirePackage[all,2cell]{xy}
\RequirePackage{mathtools}
\RequirePackage{framed}
\RequirePackage[protrusion=true,expansion=true]{microtype}
\RequirePackage{multicol}
\RequirePackage{lmodern}

\RequirePackage{geometry}
\geometry{tmargin=2cm,bmargin=2cm,lmargin=3.1cm,rmargin=3.1cm}

\RequirePackage{hyperref}

\RequirePackage{tikz}
\usetikzlibrary{calc,shapes.callouts,shapes.arrows}
\newcommand{\hcancel}[5]{%
    \tikz[baseline=(tocancel.base)]{
        \node[inner sep=0pt,outer sep=0pt] (tocancel) {#1};
        \draw[red, line width=0.3mm] ($(tocancel.south west)+(#2,#3)$) -- ($(tocancel.north east)+(#4,#5)$);
    }%
}

\setlength\parskip{\medskipamount}
\setlength\parindent{0pt}

\newlength{\titleskip}
\setlength{\titleskip}{1.5em}
\newenvironment{paper}{%
  \pagestyle{empty}%
  Lehrstuhl für Algebra und Zahlentheorie \hfill
  Wintersemester 2017/2018 \\
  Universität Augsburg \hfill
  Ingo Blechschmidt \\
}{\newpage}

\newenvironment{sheet}[2]{\begin{paper}
  \setcounter{aufgabennummer}{0}
  \begin{center}
    \Large \textbf{Übungsblatt #1 zu Garben und Logik} \\[1em]
%   \normalsize \emph{#2}
  \end{center}
  \vspace{\titleskip}}{\end{paper}}

\renewcommand*\theenumi{\alph{enumi}}
\renewcommand*\theenumii{\arabic{enumii}}
\renewcommand{\labelenumi}{(\theenumi)}
\renewcommand{\labelenumii}{\theenumii.}

\newlength{\aufgabenskip}
\setlength{\aufgabenskip}{1.5em}
\newcounter{aufgabennummer}
\newenvironment{aufgabe}[1]{
  \refstepcounter{aufgabennummer}
  \textbf{Aufgabe \theaufgabennummer{}.} \emph{#1} \par
}{\par\vspace{\aufgabenskip}}
\newenvironment{aufgabe*}[1]{
  \refstepcounter{aufgabennummer}
  \textbf{Aufgabe* \theaufgabennummer{}.} \emph{#1} \par
}{\vspace{\aufgabenskip}}
\newenvironment{aufgabeE}[1]{\begin{aufgabe}{#1}\begin{enumerate}}{\end{enumerate}\end{aufgabe}}

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

\newcommand{\C}{\mathcal{C}}

\input{macros}
