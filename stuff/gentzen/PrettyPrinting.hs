{-# LANGUAGE StandaloneDeriving #-}
module PrettyPrinting where

import Data.List
import Data.List.Split

import Types

instance Show Proof where
    show (MkProof claim rule subProofs) = concat
        [ "* "
        , show claim
        , " (" ++ rule ++ ")\n"
        , concat $ map (indent . show) subProofs
        ]
        where indent str = (++ "\n") $ concat $ intersperse "\n" $ map ("  " ++) $ splitOn "\n" $ init str

instance Show Sequent where
    show (MkSequent c phi psi) =
        showBinary ("⊢" ++ concat (intersperse "," c)) phi psi

instance Show Formula where
    show Top = "⊤"
    show Bot = "⊥"
    show (And     phi psi) = showBinary "∧" phi psi
    show (Or      phi psi) = showBinary "∨" phi psi
    show (Implies phi psi)
        | Bot <- psi = showUnary "¬" phi
        | otherwise  = showBinary "⇒" phi psi
    show (ForAll  phi)     = showUnary "∀" phi
    show (Exists  phi)     = showUnary "∃" phi
    show (Eq      s t)     = showBinary "=" s t
    show (Atomic  phi)     = phi

instance Show Term where
    show (BoundVar n) = "#" ++ show n
    show (FreeVar  x) = x
    show Zero         = "0"
    show (Succ t)     = "(succ " ++ show t ++ ")"
    show (Plus s t)   = "(" ++ show s ++ " + " ++ show t ++ ")"
    show (Mult s t)   = "(" ++ show s ++ " · " ++ show t ++ ")"

showUnary :: (Show a) => String -> a -> String
showUnary op x = concat [ "(", op, show x, ")" ]

showBinary :: (Show a) => String -> a -> a -> String
showBinary op x y = concat [ "(", show x, " ", op, " ", show y, ")" ]


-- LaTeX output

class ShowTeX a where
    showTeX :: a -> String

toTeX :: Proof -> String
toTeX p = concat $ intersperse "\n"
    [ "\\documentclass{minimal}"
    , "\\usepackage[paperheight=40cm,paperwidth=120cm]{geometry}"
    , "\\usepackage{amsmath,amssymb}"
    , "\\usepackage{tikz}\\usetikzlibrary[trees]"
--  , "\\usepackage{bussproofs}"
    , "\\begin{document}"
    , "\\tikzstyle{every node}=[draw=black,thick,anchor=west]"
    , "\\begin{tikzpicture}[grow via three points={one child at (0.5,-0.9) and two children at (0.5,-0.9) and (0.5,-1.8)},edge from parent path={(\\tikzparentnode.south) |- (\\tikzchildnode.west)}]\\node{proof}"
    , fst (treeTeX p)
    , ";\\end{tikzpicture}"
--  , "\\DisplayProof"
    , "\\end{document}"
    ]

treeTeX (MkProof claim rule subProofs) =
    let subtrees   = map treeTeX subProofs
        totalLines = length subtrees + sum (map snd subtrees)
    in ( concat
            [ "child { "
            , "node {$" ++ showTeX claim ++ "$ "
            , "(" ++ rule ++ ")}"
            , concatMap fst subtrees
            , "}"
            , concat $ replicate totalLines "child [missing] {}"
            , "\n"
            ]
        , totalLines
        )

instance ShowTeX Proof where
    showTeX (MkProof claim rule subProofs) = concat
        [ concatMap showTeX subProofs
        , "\\RightLabel{\\small " ++ rule ++ "}"
        , "\\"
        , case length subProofs of
            0 -> "AxiomC"
            1 -> "UnaryInfC"
            2 -> "BinaryInfC"
        , "{$"
        , showTeX claim
        , "$}\n"
        ]

instance ShowTeX Sequent where
    showTeX (MkSequent c phi psi) =
        showTeX phi ++ " \\mathrel{\\vdash\\!\\!\\!_{" ++ concat (intersperse "," c) ++ "}} " ++ showTeX psi

instance ShowTeX Formula where
    showTeX Top = "\\top"
    showTeX Bot = "\\bot"
    showTeX (And     phi psi) = showTeXBinary "\\wedge" phi psi
    showTeX (Or      phi psi) = showTeXBinary "\\vee" phi psi
    showTeX (Implies phi psi)
        | Bot <- psi = showTeXUnary "\\neg" phi
        | otherwise  = showTeXBinary "\\Rightarrow" phi psi
    showTeX (ForAll  phi)     = showTeXUnary "\\forall" phi
    showTeX (Exists  phi)     = showTeXUnary "\\exists" phi
    showTeX (Eq      s t)     = showTeXBinary "=" s t
    showTeX (Atomic  phi)     = phi

instance ShowTeX Term where
    showTeX (BoundVar n) = "\\#" ++ show n
    showTeX (FreeVar x)  = x
    showTeX Zero         = "0"
    showTeX (Succ t)     = "\\mathsf{succ}(" ++ showTeX t ++ ")"

showTeXUnary :: (ShowTeX a) => String -> a -> String
showTeXUnary op x = concat [ "(", op, showTeX x, ")" ]

showTeXBinary :: (ShowTeX a) => String -> a -> a -> String
showTeXBinary op x y = concat [ "(", showTeX x, " ", op, " ", showTeX y, ")" ]
