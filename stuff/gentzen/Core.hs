module Core where

import Debug.Trace
import Data.List
import qualified Data.Map as M

import Types
import PrettyPrinting
import Parser


-- Dealing with variables

substSingle :: Name -> Term -> Formula -> Formula
substSingle name t = subst $ M.fromList [(name,t)]

subst :: M.Map Name Term -> Formula -> Formula
subst env Top = Top
subst env Bot = Bot
subst env (And phi psi) = And (subst env phi) (subst env psi)
subst env (Or phi psi) = Or (subst env phi) (subst env psi)
subst env (Implies phi psi) = Implies (subst env phi) (subst env psi)
subst env (ForAll phi) = ForAll (subst env phi)
subst env (Exists phi) = Exists (subst env phi)
subst env (Eq s t) = Eq (subst' env s) (subst' env t)
subst env (Atomic phi) = Atomic phi

subst' :: M.Map Name Term -> Term -> Term
subst' env (BoundVar n) = BoundVar n
subst' env (FreeVar x)
    | Just t <- M.lookup x env
    = t
    | otherwise
    = FreeVar x
subst' env Zero = Zero
subst' env (Succ t) = Succ (subst' env t)
subst' env (Plus s t) = Plus (subst' env s) (subst' env t)
subst' env (Mult s t) = Mult (subst' env s) (subst' env t)

bind :: Name -> Formula -> Formula
bind = substIndex 0

unbind :: Term -> Formula -> Formula
unbind = insertTerm 0

insertTerm :: Nat -> Term -> Formula -> Formula
insertTerm n t Top = Top
insertTerm n t Bot = Bot
insertTerm n t (And phi psi) = And (insertTerm n t phi) (insertTerm n t psi)
insertTerm n t (Or phi psi) = Or (insertTerm n t phi) (insertTerm n t psi)
insertTerm n t (Implies phi psi) = Implies (insertTerm n t phi) (insertTerm n t psi)
insertTerm n t (ForAll phi) = ForAll (insertTerm (n+1) t phi)
insertTerm n t (Exists phi) = Exists (insertTerm (n+1) t phi)
insertTerm n t (Eq u v) = Eq (insertTerm' n t u) (insertTerm' n t v)
insertTerm n t (Atomic phi) = Atomic phi

insertTerm' :: Nat -> Term -> Term -> Term
insertTerm' n t (BoundVar m)
    | m == n    = t
    | otherwise = BoundVar m
insertTerm' n t (FreeVar x) = FreeVar x
insertTerm' n t Zero        = Zero
insertTerm' n t (Succ s)    = Succ (insertTerm' n t s)
insertTerm' n t (Plus s s') = insertTerm' n t s `Plus` insertTerm' n t s'
insertTerm' n t (Mult s s') = insertTerm' n t s `Mult` insertTerm' n t s'

substIndex :: Nat -> Name -> Formula -> Formula
substIndex n x Top = Top
substIndex n x Bot = Bot
substIndex n x (And phi psi) = And (substIndex n x phi) (substIndex n x psi)
substIndex n x (Or  phi psi) = Or  (substIndex n x phi) (substIndex n x psi)
substIndex n x (Implies phi psi) = Implies (substIndex n x phi) (substIndex n x psi)
substIndex n x (ForAll phi)  = ForAll (substIndex (n+1) x phi)
substIndex n x (Exists phi)  = Exists (substIndex (n+1) x phi)
substIndex n x (Eq t s)      = Eq (substIndex' n x t) (substIndex' n x s)
substIndex n x (Atomic phi)  = Atomic phi

substIndex' :: Nat -> Name -> Term -> Term
substIndex' n x (BoundVar i) = BoundVar i
substIndex' n x (FreeVar y)
    | x == y    = BoundVar n
    | otherwise = FreeVar y
substIndex' n x Zero = Zero
substIndex' n x (Succ t)   = Succ (substIndex' n x t)
substIndex' n x (Plus s t) = substIndex' n x s `Plus` substIndex' n x t
substIndex' n x (Mult s t) = substIndex' n x s `Mult` substIndex' n x t

occurs :: Name -> Formula -> Bool
occurs x Top = False
occurs x Bot = False
occurs x (And phi psi) = occurs x phi || occurs x psi
occurs x (Or  phi psi) = occurs x phi || occurs x psi
occurs x (ForAll phi)  = occurs x phi
occurs x (Exists phi)  = occurs x phi
occurs x (Eq t s)      = occurs' x t || occurs' x s
occurs x (Atomic phi)  = False

occurs' :: Name -> Term -> Bool
occurs' x (BoundVar _) = False
occurs' x (FreeVar y)  = x == y
occurs' x Zero         = False
occurs' x (Succ t)     = occurs' x t
occurs' x (Plus s t)   = occurs' x s || occurs' x t
occurs' x (Mult s t)   = occurs' x s || occurs' x t

substitution :: M.Map Name Term -> Proof -> Context -> Proof
substitution env p c'
    | sort (M.keys env) == sort (context (claim p))
    , all (validInContext' c' 0) (M.elems env)
    = MkProof
        (MkSequent c' (subst env (antecedent (claim p))) (subst env (consequent (claim p))))
        ("Subst[" ++ concat (intersperse "," (map (\(k,v) -> show v ++ "/" ++ k) (M.toAscList env))) ++ "]")
        [p]
    | otherwise
    = error $ show (env,p,c')

validInContext :: Context -> Nat -> Formula -> Bool
validInContext c n Top = True
validInContext c n Bot = True
validInContext c n (And phi psi) = validInContext c n phi && validInContext c n psi
validInContext c n (Or phi psi) = validInContext c n phi && validInContext c n psi
validInContext c n (Implies phi psi) = validInContext c n phi && validInContext c n psi
validInContext c n (ForAll phi) = validInContext c (n+1) phi
validInContext c n (Exists phi) = validInContext c (n+1) phi
validInContext c n (Eq s t) = validInContext' c n s && validInContext' c n t
validInContext c n (Atomic phi) = True

-- Checks whether all occuring free variables are part of the context
-- and whether only bound variables of the given depth exist.
-- validInContext' c 0 checks whether no bound variables at all occur.
validInContext' :: Context -> Nat -> Term -> Bool
validInContext' c n (BoundVar m) = m < n
validInContext' c n (FreeVar x)  = x `elem` c
validInContext' c n Zero         = True
validInContext' c n (Succ t)     = validInContext' c n t
validInContext' c n (Plus s t)   = validInContext' c n s && validInContext' c n t
validInContext' c n (Mult s t)   = validInContext' c n s && validInContext' c n t


-- Rules of Gentzen's calculus of natural deduction

refl :: Context -> Formula -> Proof
refl c phi = MkProof (MkSequent c phi phi) "Refl" []

cut :: Proof -> Proof -> Proof
cut p q =
    if c == context (claim q) && psi == psi'
        then MkProof (MkSequent c phi chi) "Cut" [p, q]
        else undefined  -- error $ show (p,q)
    where
    phi  = antecedent (claim p)
    psi  = consequent (claim p)
    psi' = antecedent (claim q)
    chi  = consequent (claim q)
    c    = context (claim p)

topI :: Context -> Formula -> Proof
topI c phi = MkProof (MkSequent c phi Top) "TopI" []

conjunctionI :: Proof -> Proof -> Proof
conjunctionI p p'
    | c  <- claim p
    , c' <- claim p'
    , context c == context c'
    , antecedent c == antecedent c'
    = MkProof
        (MkSequent (context c) (antecedent c)
            (And (consequent c) (consequent c')))
        "ConjI"
        [p, p']
    | otherwise
    = undefined

impliesI :: Proof -> Proof
impliesI p =
    case antecedent (claim p) of
        And phi psi ->
            MkProof (MkSequent (context (claim p)) phi (Implies psi (consequent (claim p)))) "ImplI" [p]
        _ -> undefined

{-
impliesE :: Proof -> Proof
impliesE p
    | phi <- antecedent (claim p)
    , Implies psi chi <- consequent (claim p)
    = MkProof (MkSequent (context (claim p)) (And phi psi) chi) "ImplE" [p]
    | otherwise
    = undefined
-}

impliesE :: Context -> Formula -> Formula -> Proof
impliesE c phi psi = MkProof (MkSequent c (phi `And` (phi `Implies` psi)) psi) "ImplE" []

conjunctionEleft :: Context -> Formula -> Proof
conjunctionEleft c f
    | And phi psi <- f
    = MkProof (MkSequent c f phi) "ConjEl" []
    | otherwise
    = undefined

conjunctionEright :: Context -> Formula -> Proof
conjunctionEright c f
    | And phi psi <- f
    = MkProof (MkSequent c f psi) "ConjEr" []
    | otherwise
    = undefined

disjunctionIleft :: Context -> Formula -> Formula -> Proof
disjunctionIleft c phi psi
    | validInContext c 0 phi
    , validInContext c 0 psi
    = MkProof (MkSequent c phi (phi `Or` psi)) "DisjIl" []
    | otherwise
    = undefined

disjunctionIright :: Context -> Formula -> Formula -> Proof
disjunctionIright c phi psi
    | validInContext c 0 phi
    , validInContext c 0 psi
    = MkProof (MkSequent c psi (phi `Or` psi)) "DisjIr" []
    | otherwise
    = undefined

forallI :: Proof -> Proof
forallI p =
    if not (null (context c)) && not (occurs name (antecedent c))
        then MkProof
            (MkSequent
                (tail (context c))
                (antecedent c)
                (ForAll $ bind name $ consequent c))
            "ForAllI"
            [p]
        else undefined
    where
    c    = claim p
    name = head (context c)

forallE :: Context -> Formula -> Proof
forallE c phi
    | (name:c') <- c
    = MkProof (MkSequent c (ForAll $ bind name phi) phi) "ForallE" []
    | otherwise
    = undefined

existsI :: Context -> Formula -> Proof
existsI c phi
    | (name:c') <- c
    = MkProof
        (MkSequent c phi (Exists $ bind name phi))
        "ExistsI"
        []
    | otherwise
    = undefined

identityI :: Context -> Term -> Proof
identityI c t = MkProof
    (MkSequent c Top (Eq t t)) "IdentityI" []

indiscernibles :: Name -> Name -> Context -> Formula -> Proof
indiscernibles x y c phi
    | x `elem` c && y `elem` c
    , validInContext c 0 phi
    = MkProof
        (MkSequent c ((FreeVar x `Eq` FreeVar y) `And` phi) (substSingle x (FreeVar y) phi))
        "indiscernibles"
        []
    | otherwise
    = undefined

mkAxiom :: String -> Formula -> Proof
mkAxiom descr phi = MkProof (MkSequent [] Top phi) descr []

axZero, axSuccessor, axPlusZero, axPlusSucc, axMultZero, axMultSucc :: Proof
axZero      = mkAxiom "zero"  $ parseFormula "¬(∃(0 = succ(#0)))"
axSuccessor = mkAxiom "succ"  $ parseFormula "∀(∀(succ(#0) = succ(#1) ⇒ #0 = #1))"
axPlusZero  = mkAxiom "plus0" $ parseFormula "∀(+(#0,0) = #0)"
axPlusSucc  = mkAxiom "plusS" $ parseFormula "∀(∀(+(#0,succ(#1)) = succ(+(#0,#1))))"
axMultZero  = mkAxiom "mult0" $ parseFormula "∀(·(#0,0) = 0)"
axMultSucc  = mkAxiom "multS" $ parseFormula "∀(∀(·(#0,succ(#1)) = +(·(#0,#1),#0)))"

axInductionRule :: Name -> Formula -> Proof -> Proof -> Proof
axInductionRule name phi base step
    | c <- context (claim base)
    , (v:c') <- context (claim step)
    , v == name && c == c'
    , antecedent (claim step) == antecedent (claim base) `And` phi
    , consequent (claim base) == substSingle name Zero phi
    , consequent (claim step) == substSingle name (Succ (FreeVar name)) phi
    = MkProof
        (MkSequent (context (claim base)) (antecedent (claim base)) $ ForAll $ bind name phi)
        "ind"
        [base,step]

axInduction :: Context -> Formula -> Proof
axInduction c phi
    | validInContext c 0 phi
    , (name:c') <- c
    = mkAxiom "ind" $
        Implies
            (And
                (substSingle name Zero phi)
                (ForAll $ bind name $ Implies phi (substSingle name (Succ (FreeVar name)) phi)))
            (ForAll $ bind name phi)


-- Enjoy
puntI :: Context -> Formula -> Formula -> Proof
puntI c gamma delta =
    trace ("punting on proof of: " ++ show (MkSequent c gamma delta)) $
        MkProof (MkSequent c gamma delta) "\27[31;1mPUNT\27[31;0m" []

suppress :: String -> Proof -> Proof
suppress descr p = MkProof (claim p) descr []
